import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";
import firebase from '../firebase';

import PageTitle from "./../components/common/PageTitle";
import SmallStats from "./../components/common/SmallStats";
import UsersOverview from "./../components/blog/UsersOverview";
import UsersByDevice from "./../components/blog/UsersByDevice";
import NewDraft from "./../components/blog/NewDraft";
import Discussions from "./../components/blog/Discussions";
import TopReferrals from "./../components/common/TopReferrals";

export default class BlogOverview extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      dataSurveys: []
    }
  }

  componentDidMount() {
    const surveyRef = firebase.database().ref('surveys');
    surveyRef.on('value', (snapshot) => {
      let surveys = snapshot.val()
      let newState = [];
      for (let survey in surveys) {
        newState.push({
          id: survey,
          kategori: surveys[survey].kategori,
          nilai: surveys[survey].nilai,
          latitude: surveys[survey].latitude,
          longitude: surveys[survey].longitude,
        })
      }
      this.setState({
        dataSurveys: newState
      })
      // console.log(newState)
      // console.log(this.state.dataSurveys)
    })
  }
  render() {
    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle title="K5-OVERVIEW" subtitle="Dashboard" className="text-sm-left mb-12" />
        </Row>

        {/* Small Stats Blocks */}
        <Row>
          {[
            {
              label: "KEBERSIHAN",
              value: this.state.dataSurveys.filter((x) => x.kategori === 'KEBERSIHAN').length,
              percentage: "",//"",
              increase: true, // true,
              chartLabels: [null, null, null, null, null, null, null],
              attrs: { md: "6", sm: "6" },
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: "rgba(0, 184, 216, 0.1)",
                  borderColor: "rgb(0, 184, 216)",
                  data: []//[1, 2, 1, 3, 5, 4, 7]
                }
              ]
            },
            {
              label: "KEINDAHAN",
              value: this.state.dataSurveys.filter((x) => x.kategori === 'KEINDAHAN').length,
              percentage: "",//"12.4",
              increase: true, // true,
              chartLabels: [null, null, null, null, null, null, null],
              attrs: { md: "6", sm: "6" },
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: "rgba(23,198,113,0.1)",
                  borderColor: "rgb(23,198,113)",
                  data: []//[1, 2, 3, 3, 3, 4, 4]
                }
              ]
            },
            {
              label: "KEAMANAN",
              value: this.state.dataSurveys.filter((x) => x.kategori === 'KEAMANAN').length,
              percentage: "",//"3.8%",
              increase: true, // false,
              decrease: false, //true,
              chartLabels: [null, null, null, null, null, null, null],
              attrs: { md: "4", sm: "6" },
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: "rgba(255,180,0,0.1)",
                  borderColor: "rgb(255,180,0)",
                  data: []//[2, 3, 3, 3, 4, 3, 3]
                }
              ]
            },
            {
              label: "KERAPIHAN",
              value: this.state.dataSurveys.filter((x) => x.kategori === 'KERAPIHAN').length,
              percentage: "",//"2.71%",
              increase: true, // false,
              decrease: false, //true,
              chartLabels: [null, null, null, null, null, null, null],
              attrs: { md: "4", sm: "6" },
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: "rgba(255,65,105,0.1)",
                  borderColor: "rgb(255,65,105)",
                  data: []//[1, 7, 1, 3, 1, 4, 8]
                }
              ]
            },
            {
              label: "KETERTIBAN",
              value: this.state.dataSurveys.filter((x) => x.kategori === 'KETERTIBAN').length,
              percentage: "",//"2.4%",
              increase: true, // false,
              decrease: false, //true,
              chartLabels: [null, null, null, null, null, null, null],
              attrs: { md: "4", sm: "6" },
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: "rgb(0,123,255,0.1)",
                  borderColor: "rgb(0,123,255)",
                  data: []//[3, 2, 3, 2, 4, 5, 4]
                }
              ]
            }
          ].map((stats, idx) => (
            <Col className="col-lg mb-4" key={idx} {...stats.attrs}>
              <SmallStats
                id={`small-stats-${idx}`}
                variation="1"
                chartData={stats.datasets}
                chartLabels={stats.chartLabels}
                label={stats.label}
                value={stats.value}
                percentage={stats.percentage}
                increase={stats.increase}
                decrease={stats.decrease}
              />
            </Col>
          ))}
        </Row>

        <Row>
          {[
            {
              label: "SANGAT BAIK",
              value: this.state.dataSurveys.filter((x) => x.nilai === 'SANGAT BAIK').length,
              percentage: "",//"",
              increase: true, // true,
              chartLabels: [null, null, null, null, null, null, null],
              attrs: { md: "6", sm: "6" },
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: "rgba(0, 184, 216, 0.1)",
                  borderColor: "rgb(0, 184, 216)",
                  data: []//[1, 2, 1, 3, 5, 4, 7]
                }
              ]
            },
            {
              label: "BAIK",
              value: this.state.dataSurveys.filter((x) => x.nilai === 'BAIK').length,
              percentage: "",//"12.4",
              increase: true, // true,
              chartLabels: [null, null, null, null, null, null, null],
              attrs: { md: "6", sm: "6" },
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: "rgba(23,198,113,0.1)",
                  borderColor: "rgb(23,198,113)",
                  data: []//[1, 2, 3, 3, 3, 4, 4]
                }
              ]
            },
            {
              label: "CUKUP",
              value: this.state.dataSurveys.filter((x) => x.nilai === 'CUKUP').length,
              percentage: "",//"3.8%",
              increase: true, // false,
              decrease: false, //true,
              chartLabels: [null, null, null, null, null, null, null],
              attrs: { md: "4", sm: "6" },
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: "rgba(255,180,0,0.1)",
                  borderColor: "rgb(255,180,0)",
                  data: []//[2, 3, 3, 3, 4, 3, 3]
                }
              ]
            },
            {
              label: "KURANG",
              value: this.state.dataSurveys.filter((x) => x.nilai === 'KURANG').length,
              percentage: "",//"2.71%",
              increase: true, // false,
              decrease: false, //true,
              chartLabels: [null, null, null, null, null, null, null],
              attrs: { md: "4", sm: "6" },
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: "rgba(255,65,105,0.1)",
                  borderColor: "rgb(255,65,105)",
                  data: []//[1, 7, 1, 3, 1, 4, 8]
                }
              ]
            }
          ].map((stats, idx) => (
            <Col className="col-lg mb-4" key={idx} {...stats.attrs}>
              <SmallStats
                id={`small-stats-${idx}`}
                variation="1"
                chartData={stats.datasets}
                chartLabels={stats.chartLabels}
                label={stats.label}
                value={stats.value}
                percentage={stats.percentage}
                increase={stats.increase}
                decrease={stats.decrease}
              />
            </Col>
          ))}
        </Row>

        <Row>
          {/* Users Overview */}
          <Col lg="8" md="12" sm="12" className="mb-4">
            <UsersOverview />
          </Col>

          {/* Users by Device */}
          <Col lg="4" md="6" sm="12" className="mb-4">
            <UsersByDevice />
          </Col>

          {/* New Draft */}
          {/* <Col lg="4" md="6" sm="12" className="mb-4">
            <NewDraft />
          </Col> */}

          {/* Discussions */}
          {/* <Col lg="5" md="12" sm="12" className="mb-4">
            <Discussions />
          </Col> */}

          {/* Top Referrals */}
          {/* <Col lg="3" md="12" sm="12" className="mb-4">
            <TopReferrals />
          </Col> */}
        </Row>
      </Container>
    )
  }
}
