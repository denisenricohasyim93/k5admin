import React from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";
import firebase from '../firebase';
import ReactAudioPlayer from 'react-audio-player';

import PageTitle from "../components/common/PageTitle";
export default class Tables extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      kumpulanVN : []
    }
  }
  async componentDidMount() {
    var storageRef = firebase.storage().ref("");
    var iniLho = this;
    await storageRef.listAll().then(async function(result) {
      await result.items.forEach(async function(audioRef) {
        await audioRef.getDownloadURL().then(async function(url) {
          // await console.log(url)
          // newState.push(url)
          // await iniLho.state.kumpulanVN.push(url)
          await iniLho.setState({
            kumpulanVN : [...iniLho.state.kumpulanVN, url]
          })
          // console.log(newState)
        }).catch(function(error) {
        });
      });
      // console.log(this.state.kumpulanVN)
    }).catch(function(error) {
    });
    await console.log(this.state.kumpulanVN)
  }

  render() {
    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle sm="4" title="K5 VOICE NOTES" subtitle="K5 KEMENDIKBUD" className="text-sm-left" />
        </Row>

        <div
          style={{display : 'flex', flexDirection : 'column'}}
        >
          {this.state.kumpulanVN.map((item, index) => {
            console.log('kuda kuda ', item)
            return (
              <div style={{display : 'flex', flexDirection : 'row', alignItems : 'center', margin : 10, borderWidth : 1, borderColor : 'black', border : 'solid', padding : 5, borderRadius : 10}}>
                {index+1}
                <ReactAudioPlayer
                  key={index}
                  src={item}
                  autoPlay={false}
                  controls
                />
              </div>
              
            )
          })}
        </div>
      </Container>  
    )
  }
}

