import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBjtJf7d8ZQFgwcoueZ0V3KAWsJI0UnVgs",
    authDomain: "k5-kemendikbud.firebaseapp.com",
    databaseURL: "https://k5-kemendikbud.firebaseio.com",
    projectId: "k5-kemendikbud",
    storageBucket: "k5-kemendikbud.appspot.com",
    messagingSenderId: "533788675545",
    appId: "1:533788675545:web:f3ea7511485aa7f3959dc8",
    measurementId: "G-FW5K88P5W9"
};

firebase.initializeApp(config);
export default firebase;